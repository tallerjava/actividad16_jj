package com.tallerjava.service;

import com.tallerjava.modelo.Cotizacion;
import com.tallerjava.repository.BackupCotizacion;
import com.tallerjava.repository.CotizacionNoGuardadaException;
import com.tallerjava.repository.CotizacionNoObtenidaException;
import com.tallerjava.repository.CotizacionNoRecuperadaException;
import com.tallerjava.repository.CotizacionRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class CotizacionService {

    private final List<CotizacionRepository> repositorios;
    private final BackupCotizacion backupCotizacion = new BackupCotizacion();
    private final static Logger LOGGER = Logger.getLogger(CotizacionService.class.getName());

    public CotizacionService(List<CotizacionRepository> repositorios) {
        this.repositorios = repositorios;
        try {
            FileHandler fileHandler = new FileHandler("serviceCotizacion.log");
            SimpleFormatter simpeFormatter = new SimpleFormatter();
            fileHandler.setFormatter(simpeFormatter);
            LOGGER.addHandler(fileHandler);
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(CotizacionService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Cotizacion> obtenerCotizaciones() {

        List<Cotizacion> cotizaciones = new ArrayList();
        Cotizacion cotizacion;
        for (CotizacionRepository repositorio : repositorios) {
            try {
                cotizacion = repositorio.obtenerCotizacion();
                cotizaciones.add(cotizacion);
                if (!"Finan Sur".equals(repositorio.getNombre())) {
                    backupCotizacion.guardarCotizacion(cotizacion);
                }
            } catch (CotizacionNoGuardadaException cotizacionNoOGuardadaException) {
                LOGGER.log(Level.SEVERE, null, cotizacionNoOGuardadaException);
            } catch (CotizacionNoObtenidaException e) {
                try {
                    cotizaciones.add(backupCotizacion.recuperarCotizacion(repositorio.getNombre()));
                } catch (CotizacionNoRecuperadaException cotizacionNoRecuperadaException) {
                    cotizaciones.add(new Cotizacion(repositorio.getNombre(), null, null, 0.0f));
                }
            }
        }
        return cotizaciones;
    }

}
