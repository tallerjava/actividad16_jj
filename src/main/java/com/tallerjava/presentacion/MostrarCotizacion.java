package com.tallerjava.presentacion;

import com.tallerjava.modelo.Cotizacion;
import com.tallerjava.repository.BinanceCotizacionRepository;
import com.tallerjava.repository.CoinDeskCotizacionRepository;
import com.tallerjava.repository.CotizacionRepository;
import com.tallerjava.repository.CryptoCotizacionRepository;
import com.tallerjava.repository.FinanSurCotizacionRepository;
import com.tallerjava.repository.SomosPNTCotizacionRepository;
import com.tallerjava.service.CotizacionService;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MostrarCotizacion", urlPatterns = {"/MostrarCotizacion"})
public class MostrarCotizacion extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<CotizacionRepository> repositorios = new ArrayList();
        repositorios.add(new CoinDeskCotizacionRepository());
        repositorios.add(new BinanceCotizacionRepository());
        repositorios.add(new SomosPNTCotizacionRepository());
        repositorios.add(new CryptoCotizacionRepository());
        repositorios.add(new FinanSurCotizacionRepository());
        Date date = new Date();
        SimpleDateFormat zonaHoraria = new SimpleDateFormat("d-M-yyyy hh:mm");
        CotizacionService service = new CotizacionService(repositorios);
        List<Cotizacion> cotizaciones = service.obtenerCotizaciones();

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>MostrarCotizacion</title>");
            out.println("<style> form { margin:0 auto;width:800px;}</style>");
            out.println("</head>");
            out.println("<body>");

            out.println("<form action=\"MostrarCotizacion\" method=\"post\">");

            out.println("<table width=\"400px\"><tr><td align=\"Center\"><font size='4'><strong>CONSULTAR COTIZACION</strong></font></td></tr>");
            out.println(" <tr><td align=\"Center\"><input type='submit' name='Cotizacion' value='Refrescar' onclick=\"document.getElementById('Refrescar').style.display = 'block';\"></td></tr>");
            out.println("</table>");
            out.println("<fieldset  style=\"color:#408080;width:400px;\"><legend align='center' style=\"color:#33F2\"><strong>Datos Cotizacion " + zonaHoraria.format(date) + "</strong></legend>");
            out.println("<table border='1'><tr><td align='center' bgcolor='#EBEBF6'>PROVEEDOR</td><td align='center' bgcolor='#EBEBF6'>MONEDA</td><td align='center' bgcolor='#EBEBF6'>PRECIO</td><td align='center' bgcolor='#EBEBF6'>FECHA</td></tr>");
            for (Cotizacion cotizacion : cotizaciones) {
                if (cotizacion.getPrecio() != 0) {
                    out.println("<tr>");
                    out.println("<td align='center' bgcolor='#FAFAFD'>");
                    out.println(cotizacion.getProveedor());
                    out.println("</td>");
                    out.println("<td align='center'>");
                    out.println(cotizacion.getMoneda());
                    out.println("</td>");
                    out.println("<td align='center'>");
                    out.format("%6.2f", cotizacion.getPrecio());
                    out.println("</td>");
                    out.println("<td align='center'>");
                    out.println(zonaHoraria.format(cotizacion.getFecha()));
                    out.println("</td>");
                    out.println("</tr>");
                } else {

                    out.println("<tr>");
                    out.println("<td align='center' bgcolor='#FAFAFD'>");
                    out.println(cotizacion.getProveedor());
                    out.println("</td>");
                    out.println("<td colspan='3' align='center'>");
                    out.println("<font color='red'>(el proveedor no se encuentra disponible)</font>");
                    out.println("</td>");
                    out.println("</tr>");
                }
            }
            out.println("</table>");
            out.println("</fieldset");

            out.println("</form>");

            out.println("<div id=\"Refrescar\" style=\"display:none;\">");
            out.println("<img style=\"margin-left: 1%;margin-top: 2%;\" alt=\"Calculando Cotizacion...\" src=\"./image/Consultar.gif\"/>");
            out.println("Refrescar Cotizaciones....");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
