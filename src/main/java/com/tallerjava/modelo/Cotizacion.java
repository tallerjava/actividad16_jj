package com.tallerjava.modelo;

import java.util.Date;

public class Cotizacion {

    private String proveedor;
    private double precio;
    private String moneda;
    private Date fecha;

    public Cotizacion(String proveedor, Date fecha, String moneda, double precio) {
        this.proveedor = proveedor;
        this.fecha = fecha;
        this.moneda = moneda;
        this.precio = precio;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

}
