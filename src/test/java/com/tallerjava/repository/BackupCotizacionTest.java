package com.tallerjava.repository;

import com.tallerjava.modelo.Cotizacion;
import static org.junit.Assert.*;
import org.junit.Test;

public class BackupCotizacionTest {

    @Test
    public void guardarCotizacion_backupOk_seGuardaLaCotizacion() {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion cotizacion = repositorio.obtenerCotizacion();
        BackupCotizacion backup = new BackupCotizacion();
        backup.guardarCotizacion(cotizacion);
    }

    @Test(expected = CotizacionNoGuardadaException.class)
    public void guardarCotizacion_backupFalla_cotizacionNoGuardadaException() {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion cotizacion = repositorio.obtenerCotizacion();
        BackupCotizacion backup = new BackupCotizacion();
        backup.setUsuario("User");
        backup.guardarCotizacion(cotizacion);
    }

    @Test
    public void recuperarCotizacion_proveedorCoindeskBackupOk_seRecuperaLaCotizacion() {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion resultadoEsperado = repositorio.obtenerCotizacion();
        BackupCotizacion backup = new BackupCotizacion();
        backup.guardarCotizacion(resultadoEsperado);
        Cotizacion resultadoObtenido = backup.recuperarCotizacion(repositorio.getNombre());
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = CotizacionNoRecuperadaException.class)
    public void recuperarCotizacion_proveedorCoindeskBackupFalla_cotizacionNoRecuperadaException() {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        Cotizacion cotizacion = repositorio.obtenerCotizacion();
        BackupCotizacion backup = new BackupCotizacion();
        backup.setUsuario("User");
        backup.recuperarCotizacion(cotizacion.getProveedor());
    }

}
